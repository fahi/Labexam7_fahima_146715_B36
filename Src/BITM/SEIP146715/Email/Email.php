<?php
namespace App\Email;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;


class Email extends DB
{
    public $id = "";
    public $user_email = "";
    public $password = "";

    public function __construct()
    {
        parent:: __construct();
        if (!isset($_SESSION)) session_start();
    }// end of __construct()


    public function setData($postVariableData=NULL){

        if( array_key_exists("id",$postVariableData) ){

            $this->id     =  $postVariableData['id'];
        }

        if( array_key_exists("email",$postVariableData) ){

            $this->user_email    =  $postVariableData['email'];
        }

        if( array_key_exists("password",$postVariableData) ){

            $this->password   =  $postVariableData['password'];
        }
    }// end of setData()



    public function store(){
        $arrData = array($this->user_email,$this->password);
        $sql = "insert into email(user_email,password) VALUES(?,?)";
        $STH = $this->DBH->prepare($sql);
        $result =$STH->execute($arrData );
        Utility::redirect('create.php');

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");







    }// end of store()


}//  end of Email Class