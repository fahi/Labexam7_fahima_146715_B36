<?php
namespace App\BookTitle;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;


class BookTitle extends DB
{
    public $id = "";
    public $book_title = "";
    public $author_name = "";

    public function __construct()
    {
        parent:: __construct();
        if (!isset($_SESSION)) session_start();
    }// end of __construct()


    public function setData($postVariableData=NULL){

        if( array_key_exists("id",$postVariableData) ){

            $this->id     =  $postVariableData['id'];
        }

        if( array_key_exists("book_title",$postVariableData) ){

            $this->book_title     =  $postVariableData['book_title'];
        }

        if( array_key_exists("author_name",$postVariableData) ){

            $this->author_name   =  $postVariableData['author_name'];
        }
    }// end of setData()



    public function store(){
        $arrData = array($this->book_title,$this->author_name);
        $sql = "insert into book_title(book_title,author_name) VALUES(?,?)";
        $STH = $this->DBH->prepare($sql);
        $result =$STH->execute($arrData );
        Utility::redirect('create.php');

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");







    }// end of store()


}//  end of BookTitle Class