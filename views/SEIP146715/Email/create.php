<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo Message::message();

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Email - Formoid form builder</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body class="blurBg-false" style="background-color:#3fd4eb">



<!-- Start Formoid form-->
<link rel="stylesheet" href="../../../resource/email_files/formoid1/formoid-solid-green.css" type="text/css" />
<script type="text/javascript" src="../../../resource/email_files/formoid1/jquery.min.js"></script>
<form class="formoid-solid-green" style="background-color:#FFFFFF;font-size:14px;font-family:'Roboto',Arial,Helvetica,sans-serif;color:#34495E;max-width:480px;min-width:150px"    action="store.php" method="post"><div class="title"><h2>Email</h2></div>
    <div class="element-email"><label class="title"></label><div class="item-cont"><input class="large" type="email" name="email" value="" placeholder="Email"/><span class="icon-place"></span></div></div>
    <div class="element-password"><label class="title"></label><div class="item-cont"><input class="large" type="password" name="password" value="" placeholder="Password"/><span class="icon-place"></span></div></div>
    <div class="submit"><input type="submit" value="Submit"/></div></form><p class="frmd"><a href="http://formoid.com/v29.php">form builder</a> Formoid.com 2.9</p><script type="text/javascript" src="../../../resource/email_files/formoid1/formoid-solid-green.js"></script>
<!-- Stop Formoid form-->



</body>
</html>
