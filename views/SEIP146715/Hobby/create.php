<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

echo Message::message();



?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Hobby - Formoid jquery form validation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body class="blurBg-true" style="background-color:#dbe1d6">



<!-- Start Formoid form-->
<link rel="stylesheet" href="../../../resource/hobby_files/formoid1/formoid-solid-light-green.css" type="text/css" />
<script type="text/javascript" src="../../../resource/hobby_files/formoid1/jquery.min.js"></script>
<form class="formoid-solid-light-green" style="background-color:#ffffff;font-size:14px;font-family:Tahoma,Geneva,sans-serif;color:#34495E;max-width:480px;min-width:150px"  action="store.php" method="post"><div class="title"><h2>Hobby</h2></div>
    <div class="element-input"><label class="title"></label><div class="item-cont"><input class="large" type="text" name="input" placeholder="Enter User Name"/><span class="icon-place"></span></div></div>
    <div class="element-checkbox">
        <label class="title">Select Hobby<span class="required">*</span></label>
        <div class="column column3"><label>
                <input type="checkbox" name="checkbox[]" value="Singing"/ required="required"><span>Singing</span></label></div><span class="clearfix"></span>
        <div class="column column3"><label><input type="checkbox" name="checkbox[]" value="Dancing"/ required="required"><span>Dancing</span></label></div><span class="clearfix"></span>
        <div class="column column3"><label><input type="checkbox" name="checkbox[]" value="Drawing"/ required="required"><span>Drawing</span></label></div><span class="clearfix"></span>
    </div>
    <div class="submit"><input type="submit" value="Add"/></div></form><p class="frmd"><a href="http://formoid.com/v29.php">jquery form validation</a> Formoid.com 2.9</p><script type="text/javascript" src="../../../resource/hobby_files/formoid1/formoid-solid-light-green.js"></script>
<!-- Stop Formoid form-->



</body>
</html>
